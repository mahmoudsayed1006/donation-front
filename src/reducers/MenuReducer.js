import {SELECT_MENU} from '../actions/types';
const initState = {
    key:1,
    select:true,
}

const MenuReducer = (state=initState,action) => {
    switch(action.type){
        case SELECT_MENU:
        console.log("select   ",action.isSelected,"    ",action.payload)
        return {...state,key:action.payload,select:action.isSelected}
        default:
        return state;
    }
}

export default MenuReducer;