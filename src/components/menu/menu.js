import React, { Component } from 'react';
import './menu.css';
import { connect } from 'react-redux'
import  {allStrings} from '../../assets/strings'
//import {Button, SideNav ,SideNavItem,Icon,Collapsible,CollapsibleItem} from 'react-materialize'
import { Menu, Icon} from 'antd';
import "antd/dist/antd.css";
import {SelectMenuItem } from '../../actions/MenuAction'

const { SubMenu } = Menu;

class AppMenu extends Component {
  constructor(props){
    super(props)
    if(this.props.isRTL){
      allStrings.setLanguage('ar')
    }else{
      allStrings.setLanguage('en')
    }
  }

  state = {
    collapsed: false,
  }

  toggleCollapsed = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render() {
     const {goTo,height} = this.props;
     console.log("GO TO   ",goTo)
    const x  = !this.props.select;
    return (
      <div style={{direction:"rtl"}} >
    
         <Menu  
         style={{position:'fixed',background:'187336'}}       
          //defaultSelectedKeys={[this.props.key]}
          //defaultOpenKeys={['sub1']}
          mode="inline" 
          theme="dark"
          inlineCollapsed={this.props.select}
        >
          <Menu.Item  style={{marginTop: "14px"}}
              onClick={()=>{
               
              this.props.SelectMenuItem(0,x)
              }} >
              <Icon type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'} />
              
          </Menu.Item>

          <Menu.Item
            style={{}}
            onClick={()=>{
              goTo.push('/Dashboard')
              //alert("")
              console.log("GO TO   ",goTo)
              this.props.SelectMenuItem(1,true)
            }}
            key="1" style={{marginBottom:6}}>
            <Icon type="home"   style={{fontSize:20,color:'#fff'}} />
              <span  > {allStrings.home}</span>
          </Menu.Item>
          
          <Menu.Item
              onClick={()=>{
                goTo.push('/category')
                //alert("")
                console.log("GO TO   ",goTo)
                this.props.SelectMenuItem('7',true)
              }}
              key="70" style={{marginBottom:6}}>        
              <Icon type="appstore"   style={{fontSize:20,color:'#fff'}} />
                <span  > {allStrings.category}</span>
          </Menu.Item>
          <Menu.Item
              onClick={()=>{
                goTo.push('/Volunteers')
                //alert("")
                console.log("GO TO   ",goTo)
                this.props.SelectMenuItem('71',true)
              }}
              key="71" style={{marginBottom:6}}>        
              <Icon type="usergroup-add"   style={{fontSize:20,color:'#fff'}} />
                <span  > {allStrings.volunteers}</span>
          </Menu.Item>
          <Menu.Item
              onClick={()=>{
                goTo.push('/Needys')
                //alert("")
                console.log("GO TO   ",goTo)
                this.props.SelectMenuItem('72',true)
              }}
              key="72" style={{marginBottom:6}}>        
              <Icon type="user-delete"   style={{fontSize:20,color:'#fff'}} />
                <span  > {allStrings.needys}</span>
          </Menu.Item>
          <Menu.Item
              onClick={()=>{
                goTo.push('/Donation')
                //alert("")
                console.log("GO TO   ",goTo)
                this.props.SelectMenuItem('73',true)
              }}
              key="73" style={{marginBottom:6}}>        
              <Icon type="dollar"   style={{fontSize:20,color:'#fff'}} />
                <span  > {allStrings.Donations}</span>
          </Menu.Item>

        </Menu>

      </div>

    );
  }
}
const mapToStateProps = state => ({
  isRTL: state.lang.isRTL,
  currentUser: state.auth.currentUser,
  key: state.menu.key,
  select: state.menu.select
})

const mapDispatchToProps = {
  SelectMenuItem,
}

export default connect(mapToStateProps,mapDispatchToProps) (AppMenu);
