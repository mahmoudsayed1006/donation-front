
import React, { Component } from 'react';
import './nav.css';
import {Icon,Input} from 'react-materialize';
import { List, Avatar,Dropdown,Menu,Button,Form,Modal,message} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import { connect } from 'react-redux';
import {BASE_END_POINT} from '../../config/URL'
import {withRouter} from 'react-router-dom'
import {logout} from '../../actions/LogoutActions'
import  {allStrings} from '../../assets/strings'
import {ChangeLanguage} from '../../actions/LanguageAction'
import {NavLink} from 'react-router-dom'



class Nav extends Component {
   page =1;
   MSGpages =1;
   peoplePages=1;

    state = {
     
    }

    constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }

    componentDidMount(){
     
    }

  render() {
    const {logout, currentUser, userToken,history} = this.props;
    //console.log(this.props.isRTL)

        const menu2 = (
          
          <Menu>
             {this.props.currentUser && 
            <Menu.Item
            onClick={()=> this.props.history.push(this.props.currentUser?'/UserInfo':'/Login',{data:this.props.currentUser.user})}
             key="0">{allStrings.profile}</Menu.Item>
            }
            {this.props.currentUser&& this.props.currentUser.user.type !="ADMIN" &&
             <Menu.Item key="4"  onClick={()=> this.props.history.push('/Home')}>
               {allStrings.Home}
            </Menu.Item>
            }
             {this.props.currentUser&& 
            <Menu.Item 
            onClick={()=>logout(userToken, currentUser.token, history)}
             key="1">{allStrings.logout}</Menu.Item>
             }
            <Menu.Divider />

             <Menu.Item 
            onClick={()=>{
              this.props.ChangeLanguage(true)
              allStrings.setLanguage('ar')
              localStorage.setItem('@lang','ar'); 
            }}
             key="2">{allStrings.arabic}</Menu.Item>
             <Menu.Item 
            onClick={()=>{
              this.props.ChangeLanguage(false)
              allStrings.setLanguage('en')
              localStorage.setItem('@lang','en');
            }}
             key="3">{allStrings.english}</Menu.Item>
              
          </Menu>
        );
    
    return (
      <nav>
        
      
        <div className={`nav-wrapper ${!this.props.select&&'nav-wrapper-active'}`}>
          <ul className="right hide-on-med-and-down">
            <li className='action'>
            <Dropdown overlay={menu2} trigger={['click']}>
              <a className="ant-dropdown-link" href="#">
              <Icon>more_vert</Icon>
              </a>
            </Dropdown>
            </li>
          </ul>
          <ul className="left hide-on-med-and-down">
            <li>
              <img style={{ marginLeft: '7rem'}} width= "100px"src="https://res.cloudinary.com/boody-car/image/upload/v1596675825/xcnkmnhdks3mxivcrsqm.png"></img>
            </li>
          </ul>
        </div>
      </nav> 
    );
  }
}

const mapToStateProps = state => ({
  isRTL: state.lang.isRTL,
  currentUser: state.auth.currentUser,
  userToken: state.auth.userToken,
  select: state.menu.select
})

const mapDispatchToProps = {
  logout,
  ChangeLanguage
}

export default withRouter(connect(mapToStateProps,mapDispatchToProps)(Nav = Form.create({ name: 'normal_login' })(Nav)));

