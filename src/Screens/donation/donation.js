import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';
import './donation.css';
import {Carousel} from 'react-materialize';
import "antd/dist/antd.css";
 import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import {  message,Modal, Form, Input,Icon} from 'antd';
import {Table} from 'react-materialize'
import {NavLink} from 'react-router-dom'

class Donation extends React.Component {
    state = {
       categories:[]         
   }
      getCategories = () =>{
          axios.get(`${BASE_END_POINT}categories`)
          .then(response=>{
              console.log('CATEGORIES  ',response.data.data)
              const cat = response.data.data;
              this.setState({categories:cat}) 
          })
          .catch(error=>{
            console.log('CATEGORIES ERROR  ',error.response)
          })
      }

         constructor(props){
          super(props)
          if(this.props.isRTL){
            allStrings.setLanguage('ar')
          }else{
            allStrings.setLanguage('en')
          }
        }
        
        
         componentDidMount()
         {
            this.getCategories()
         }

   
    
    render() {
        const { getFieldDecorator } = this.props.form;
        const {select,isRTL} = this.props;
        const {categories} = this.state
      return (
          
        <div>
         <AppMenu height={'150%'} goTo={this.props.history}></AppMenu>
          <Nav></Nav>
          <div style={{marginRight:!select?'20.2%':'5.5%'}} className='menu'>
            <div className='login'>
              <div class="row">
                <div class="col m2">
                  <div class="row" style={{marginTop:"0px",padding: "40px",textAlign:'center'}}>
                    <div style={{width:'100%'}}>
                    {categories.map(category=>(
                      <NavLink to={`/Donations/${category.id}`}>
                      <a class="waves-effect waves-light btn btn-large warning">{isRTL?category.arabicname:category.categoryname}</a>
                      </NavLink>
                    ))} 
                  </div>
                  </div>
                </div>
              </div>
            </div>
            <Footer></Footer>
            </div>
        </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,
  })
  
  const mapDispatchToProps = {
  }

export default connect(mapToStateProps,mapDispatchToProps)(Donation = Form.create({ name: 'normal_login', })(Donation)) ;
