import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './donation info.css';
import {Carousel} from 'react-materialize';
import "antd/dist/antd.css";
 import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import {  message,Modal, Form, Input,Icon} from 'antd';
import {Table} from 'react-materialize'

class DonationInfo extends React.Component {
    state = {
        modal1Visible: false,
         Donation:this.props.location.state.data,
         flag:this.props.location.state.flag,
         file: this.props.location.state.data.img,
         }

         constructor(props){
          super(props)
          if(this.props.isRTL){
            allStrings.setLanguage('ar')
          }else{
            allStrings.setLanguage('en')
          }
        }
        
         onChange = (e) => {
            this.setState({file:e.target.files[0]});
        }
        
         componentDidMount()
         {
             //console.log("ANWEr")
             //console.log(this.props.currentUser)
            
             //this.props.location.state.data
             //console.log(this.props.location.state.data)
         }

   
    
    deleteDonation = () => {
        let l = message.loading(allStrings.wait, 2.5)
        axios.delete(`${BASE_END_POINT}posts/${this.state.Donation.id}`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            l.then(() => message.success(allStrings.deleteDone, 2.5))
            this.props.history.goBack()

        })
        .catch(error=>{
            console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })
     }
     acceptDonation = () => {
      let l = message.loading(allStrings.wait, 2.5)
      axios.put(`${BASE_END_POINT}posts/${this.state.Donation.id}/accept`,{
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${this.props.currentUser.token}`
        },
      })
      .then(response=>{
          l.then(() => message.success(allStrings.done, 2.5))
          this.props.history.goBack()

      })
      .catch(error=>{
          console.log(error.response)
          l.then(() => message.error('Error', 2.5))
      })
   }

 
     
      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }
    render() {
        const { getFieldDecorator } = this.props.form;
         //select
         const {Donation} = this.state

        const {select} = this.props;
      return (
          
        <div>
         <AppMenu height={'150%'} goTo={this.props.history}></AppMenu>
          <Nav></Nav>
          <div style={{marginRight:!select?'20.2%':'5.5%'}} className='menu'>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#187336'}} >
                    <h2 class="center-align" style={{color:'#fff',marginTop:"10px"}}>{allStrings.donation}</h2>
                </div>
                <div class="row" style={{marginTop:"0px",padding: "40px"}}>
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={Donation.id}>
                            </input>
                            <label for="name" class="active">{allStrings.id}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={Donation.user.fullname}>
                            </input>
                            <label for="name" class="active">{allStrings.fullname}</label>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={Donation.description}>
                            </input>
                            <label for="name" class="active">{allStrings.description}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={Donation.accept}>
                            </input>
                            <label for="name" class="active">{allStrings.accepted}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={Donation.phone}>
                            </input>
                            <label for="name" class="active">{allStrings.phone}</label>
                            </div>
                        </div>
                            <a class="waves-effect waves-light btn btn-large delete " onClick={this.deleteDonation}>{allStrings.remove}</a>
                            <a class="waves-effect waves-light btn btn-large edit " onClick={this.acceptDonation}>{allStrings.accepted}</a>
                        </form>
                    </div>
            </div>
        </div>
        </div>
        <Footer></Footer>
        </div>
    </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,
  })
  
  const mapDispatchToProps = {
  }

export default connect(mapToStateProps,mapDispatchToProps)(DonationInfo = Form.create({ name: 'normal_login', })(DonationInfo)) ;
