import React ,{ Fragment }from 'react';


import './sign up.css';
import { Skeleton, message,Modal, Form, Input, Button,Popconfirm,Steps,Select ,Icon} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import {signup} from '../../actions/AuthActions'
import { MDBContainer,  MDBRow, MDBCol,MDBInput} from "mdbreact";
import Nav from '../../components/navbar/navbar';
import {NavLink} from 'react-router-dom';
const { Step } = Steps;
const { Texttype } = Input;

let user = new FormData();

class SignUp extends React.Component {
  componentDidMount() {
    window.scrollTo(0, 0)
  }
  state = {
    username:' ',
    fullname:' ',
    email:' ',
    phone:' ',
    password:' ',
    confirmPassword:' ',
    description:' ',
  };

     constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }
    
    handleSubmit = e => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          if(values.description){
            user.append('description',values.description)
          }
         user.append('username',values.username)
         user.append('fullname',values.fullname)
         user.append('email',values.email)
         user.append('phone',values.phone)
         user.append('password',values.password)
         user.append('type','VOLUNTEER')
         this.props.signup(user,this.props.history)
        }
        
      });
    };
  
  

    render() {
      const { getFieldDecorator } = this.props.form;
      const {isRTL} = this.props
      const {username,fullname,phone,password,email,description} = this.state
      const Option = Select.Option;

          
      return ( 
        <div className="loginBack3"> 
          <Nav></Nav>
            <MDBContainer> 
            <MDBRow>
              <div className='signUp loginn'>
              <Form onSubmit={this.handleSubmit} className="login-form">  
              <div>
                <Form.Item >
                {getFieldDecorator('username', {
                    rules: [
                      { required: true, 
                        message: isRTL?allStrings.usernameValid:allStrings.usernameValid 
                       },
                    ],
                    initialValue:username.replace(/\s/g, '').length?username:null
                })(
                    <Input onChange={(e)=>{
                       this.setState({username:e.target.value})
                    }} placeholder={allStrings.username} />
                )}
                </Form.Item>  
                <Form.Item >
                {getFieldDecorator('fullname', {
                    rules: [
                      { required: true, 
                        message: isRTL?allStrings.fullnameValid:allStrings.fullnameValid      

                       },
                    ],
                    initialValue:fullname.replace(/\s/g, '').length?fullname:null
                })(
                    <Input onChange={(e)=>{
                       this.setState({fullname:e.target.value})
                    }} placeholder={allStrings.fullname} />
                )}
                </Form.Item>  
                <Form.Item>
                {getFieldDecorator('password', {
                    rules: [{ required: true,  message: isRTL?allStrings.passwordValid:allStrings.passwordValid }],
                    initialValue:password.replace(/\s/g, '').length?password:null
                })(
                    <Input.Password 
                    onChange={(e)=>{
                      this.setState({password:e.target.value})
                   }}
                     placeholder={allStrings.password} />
                )}
                </Form.Item>
               
                <Form.Item>
                {getFieldDecorator('phone', {
                    rules: [
                      { required: true, message: isRTL?allStrings.phoneValid:allStrings.phoneValid },
                      //{type: 'number',  message: 'Please enter numbers' }
                    ],
                    initialValue:phone.replace(/\s/g, '').length?phone:null
                })(
                    <Input onChange={(e)=>{
                      this.setState({phone:e.target.value})
                   }} placeholder={allStrings.phone} />
                )}
                </Form.Item>
                <Form.Item>
                {getFieldDecorator('email', {
                    rules: [
                      { required: true, message: isRTL?allStrings.emailValid:allStrings.emailValid },
                      //{type: 'number',  message: 'Please enter numbers' }
                    ],
                    initialValue:email.replace(/\s/g, '').length?email:null
                })(
                    <Input onChange={(e)=>{
                      this.setState({email:e.target.value})
                   }} placeholder={allStrings.email} />
                )}
                </Form.Item>
                <Form.Item>
                {getFieldDecorator('description', {
                    rules: [
                      { required: false, message: isRTL?allStrings.descriptionValid:allStrings.descriptionValid },
                      //{type: 'number',  message: 'Please enter numbers' }
                    ],
                    initialValue:description.replace(/\s/g, '').length?description:null
                })(
                    <Input onChange={(e)=>{
                      this.setState({description:e.target.value})
                   }} placeholder={allStrings.description} />
                )}
                </Form.Item>

              </div>   
             
              <div>
                <p style={{textAlign:'center', color:'#000',marginTop:'30px',fontٍSize:"16px"}}>
                <NavLink to='/Login'>i have an account sign in</NavLink>
                <hr></hr>
                </p>
              </div>
                </Form>
                <Button className='loginnBtn' onClick={this.handleSubmit}>{allStrings.SignUp}</Button>            
              </div>
            </MDBRow>
            </MDBContainer>
        </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
    signup,
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(SignUp = Form.create({ name: 'normal_login' })(SignUp));
