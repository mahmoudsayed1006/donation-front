import React, { Component } from 'react';
import { Route, BrowserRouter, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'
import {store,persistor } from '../store';
import { PersistGate } from 'redux-persist/integration/react'
import Home from './home/home';
import Login from './login/login';
import SignUpNeedy from './sign up needy/sign up';
import SignUpVolunteer from './sign up volunteer/sign up';
import CategoryInfo from './category info/category info';
import Category from './category/category';
import UserInfo from './user info/user info';
import User from './users/users';
import Volunteer from './volunteer/users';
import Donation from './donation/donation';
import Donations from './donations/donations';
import DonationInfo from './donation info/donation info';

import Splash from './splash/splash';
import Dashboard from './dashboard/dashboard'
class App extends Component {
  componentDidMount(){
   // askForPermissioToReceiveNotifications() 
  }
  //
  render() {
    return (
      <Provider store={store}>
         <PersistGate loading={null} persistor={persistor}>
      <BrowserRouter>
        <div className="App">
          <Switch>
          <Route exact path="/" component={Splash} />
            <Route path="/Home" component={Home} />
            <Route path="/Donation" component={Donation} />
            <Route path="/Donations/:id" component={Donations} />
            <Route path="/DonationInfo" component={DonationInfo} />
            <Route path="/Dashboard" component={Dashboard} />
            <Route path='/Login' component={Login}/>
            <Route path='/SignUpNeedy' component={SignUpNeedy}/>
            <Route path='/SignUpVolunteer' component={SignUpVolunteer}/>
            <Route path='/Category' component={Category}/>
            <Route path='/Needys' component={User}/>
            <Route path='/Volunteers' component={Volunteer}/>
            <Route path='/UserInfo' component={UserInfo}/>
            
            <Route path='/CategoryInfo' component={CategoryInfo}/>
           
          </Switch>
        </div>
      </BrowserRouter>
      </PersistGate>
      </Provider>
    );
  }
}

export default App;
