import React from 'react';
import AppMenu from '../../components/menu/menu';

import Nav from '../../components/navbar/navbar';
import Tables from '../../components/table/table';
import Footer from '../../components/footer/footer';

import './donations.css';
import { Skeleton, message,Modal, Form, Icon, Input, Button,Popconfirm,Select} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'


class Donations extends React.Component {
  page = 0;
  pagentationPage=0;
  counter=0;
  state = {
    modal1Visible: false,
    confirmDelete: false,
    selectedDonations:null,
     posts:[],
     parents:[],
     file:null,
     loading:true,
     tablePage:0,
     }

     constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }

     onChange = (e) => {
      this.setState({file:e.target.files[0]});
     
  }
    //submit form
    flag = -1;
    getposts = (page,deleteRow) => {
      axios.get(`${BASE_END_POINT}posts?category=${this.props.match.params.id}&page=${page}&limit=20`)
      .then(response=>{
        console.log("ALL posts    ",response.data)
        console.log(response.data)
        if(deleteRow){
          this.setState({tablePage:0})
         }
        this.setState({posts:deleteRow?response.data.data:[...this.state.posts,...response.data.data],loading:false})
      })
      .catch(error=>{
        console.log("ALL posts ERROR")
        console.log(error.response)
        this.setState({loading:false})
      })
    }
  
    componentDidMount(){
      this.getposts(1,true)
    }
    OKBUTTON = (e) => {
      this.deleteDonations()
     }

     deleteDonations = () => {
       let l = message.loading(allStrings.wait, 2.5)
       axios.delete(`${BASE_END_POINT}posts/${this.state.selectedDonations}`,{
         headers: {
           'Content-Type': 'application/json',
           'Authorization': `Bearer ${this.props.currentUser.token}`
         },
       })
       .then(response=>{
           l.then(() => message.success(allStrings.deleteDone, 2.5))
           this.getposts(1,true)
           this.flag = -1
       })
       .catch(error=>{
          // console.log(error.response)
           l.then(() => message.error('Error', 2.5))
       })
    }
//end modal
    render() {
        //form
         const { getFieldDecorator } = this.props.form;
         //select
         const Option = Select.Option;

          let controls = (
            <Popconfirm
            title={allStrings.areYouSure}
            onConfirm={this.OKBUTTON}
            onCancel={this.fCANCELBUTTON}
            okText={allStrings.ok}
            cancelText={allStrings.cancel}
          >
             <Icon className='controller-icon' type="delete" />
          </Popconfirm>
         )
        
          let list =this.state.posts.map((val,index)=>[
            val.id,val.description,val.phone
           , controls
          ])

          const loadingView = [
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            
           ]
           const {select} = this.props;
      return (
          <div>
              <AppMenu height={'140%'} goTo={this.props.history}></AppMenu>
              <Nav></Nav>
              <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}} className='menu'>
              <Tables
               columns={this.state.loading?['loading...']: [allStrings.id,allStrings.description,allStrings.phone, allStrings.remove]} 
              title={allStrings.Donations}
              page={this.state.tablePage}
              onCellClick={(colData,cellMeta,)=>{
                //comment
                //console.log('col index  '+cellMeta.colIndex)
                //console.log('row index   '+colData)
                if(cellMeta.colIndex!==3){
                  
                  //console.log(this.state.posts[cellMeta.rowIndex])
                  this.props.history.push('/DonationInfo',{data:this.state.posts[this.pagentationPage+cellMeta.rowIndex]})
                }else if(cellMeta.colIndex===3){
                    const id = list[ this.pagentationPage+cellMeta.rowIndex][0];
                    this.setState({selectedDonations:id})
                    //console.log(id)
                  }
              }}
               onChangePage={(currentPage)=>{
                this.setState({tablePage:currentPage})
                if(currentPage>this.counter){
                  this.counter=currentPage;
                  this.pagentationPage=this.pagentationPage+10
                }else{
                 this.counter=currentPage;
                 this.pagentationPage=this.pagentationPage-10
                }
                //console.log(currentPage)
                if(currentPage%2!==0  && currentPage > this.flag){
                  this.getposts(currentPage+1)
                  this.flag  = currentPage;
                 
                }
                  
              }}
              arr={this.state.loading?loadingView:list}></Tables>
              
            </div>
             <Footer></Footer>
          </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(Donations = Form.create({ name: 'normal_login' })(Donations));
