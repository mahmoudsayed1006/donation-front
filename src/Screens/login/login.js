import React ,{ Fragment }from 'react';
import {Icon} from 'react-materialize';
import './login.css';
import { Skeleton, message,Modal, Form, Input, Button,Popconfirm, Alert} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import { MDBContainer,  MDBRow, MDBCol,MDBInput } from "mdbreact";
import Nav from '../../components/navbar/navbar';

import {NavLink} from 'react-router-dom';
import {login,signup} from '../../actions/AuthActions'


class Login extends React.Component {

     constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }

    handleSubmit = e => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          const user = {
            username: values.username,
            password: values.password,
            token:''
          }
          this.props.login(user,this.props.history)
        }
      });
    };
    
  
    render() {
      const { getFieldDecorator } = this.props.form;
      const {isRTL} = this.props
          
      return ( 
        <div className="loginBack"> 
            <Nav></Nav>
            <MDBContainer>

            <MDBRow>
              
              <div className='loginn'>
                
                <Form onSubmit={this.handleSubmit} className="login-form">                                      
                    <Form.Item>
                    {getFieldDecorator('username', {
                        rules: [
                          { required: true, message: isRTL?allStrings.usernameValid:allStrings.usernameValid},
                        ],
                    })(
                        <Input placeholder={allStrings.username} />
                    )}
                    </Form.Item>

                    <Form.Item>
                    {getFieldDecorator('password', {
                        rules: [{ required: true,  message: isRTL?allStrings.passwordValid:allStrings.passwordValid }],
                    })(
                        <Input.Password placeholder={allStrings.password} />
                    )}
                    </Form.Item>
                </Form>
                <Button className='loginnBtn' onClick={this.handleSubmit}>{allStrings.login}</Button>
                
                <div className="social">
                  <p style={{textAlign:'center', color:'#000',marginTop:'30px',fontٍSize:"16px"}}>
                    <span>{allStrings.noAccountSignUp}</span>
                    <NavLink to='/SignUpNeedy'>{allStrings.SignUpNeedy}</NavLink>
                    <span>{allStrings.Or}</span>
                    <NavLink to='/SignUpVolunteer'>{allStrings.SignUpVolunteer}</NavLink>
                <hr></hr>
                </p>
                </div>

               
              </div>
            
            </MDBRow>
            </MDBContainer>
            
            {/*<Footer></Footer>*/}
        </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    loading: state.auth.loading,
    currentUser: state.auth.currentUser,
    //select: state.menu.select,

  })
  
  const mapDispatchToProps = {
    login,
    signup,
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(Login = Form.create({ name: 'normal_login' })(Login));
