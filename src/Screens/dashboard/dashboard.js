import React, { Component } from 'react';
import Nav from '../../components/navbar/navbar';
import AppMenu from '../../components/menu/menu';

import './dashboard.css';
import {Icon,Table} from 'react-materialize'
import {NavLink} from 'react-router-dom';
import {Skeleton} from 'antd';
import 'antd/dist/antd.css';
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import {withRouter} from 'react-router-dom'


class Dashboard extends Component {
   
  state = {
    loading:true,
    loading2:true,
    counts:[]
   
}

constructor(props){
    super(props)
    if(this.props.isRTL === true){
      allStrings.setLanguage('ar')
    }else{
      allStrings.setLanguage('en')
    }

  }
  
 
    componentDidMount(){
       // console.log(this.props.currentUser)
        console.log("push   ",this.props.history)
    }
      


  render() {
      const loadingView = 
       <Skeleton  active/> 

       const{select} = this.props
       //alert(''+window.innerHeight)
    return (
      <div style={{paddingBottom: '100px'}}>
        
        <AppMenu height={'140%'} goTo={this.props.history}></AppMenu>

        <Nav></Nav>

        
        <div className='menu'  style={{marginRight:!select?'20.2%':'5.5%'}}>
          <div className='content' >
            <h1 className='dashTitle'>{allStrings.welcome}</h1>
       
              <div className="row">
                  <div className="col s12 m6 xl4 l6 count1">
                  <NavLink to='/Volunteers'>
                  <div className="icons">
                      <Icon>group</Icon>
                  </div>
                  <div className='info' >
                      <p>{allStrings.volunteers}</p>
                  </div>
                  </NavLink>
                  </div>         
                  <div className="col s12 m6 xl4 l6 count2">
                  <NavLink to='/Donation'>
                      <div className="icons">
                      <Icon>attach_money</Icon> 
                      </div>
                      <div className='info'>
                          <p>{allStrings.Donations}</p>
                      </div>
                  </NavLink>
                  </div>
                  <div className="col s12 m6 xl4 l6 count3">
                  <NavLink to='/Needys'>
                      <div className="icons">
                      <Icon>group</Icon>
                      </div>
                      <div className='info'>
                          <p>{allStrings.needys}</p>
                      </div>
                  </NavLink>
                  </div> 
                  <div className="col s12 m6 xl4 l6 count1">
                  <NavLink to='/category'>
                      <div className="icons">
                      <Icon>list</Icon>
                      </div>
                      <div className='info'>
                          <p>{allStrings.categories}</p>
                      </div>
                  </NavLink>
                  </div> 
              </div>
          </div>
          </div>
      </div>
    );
  }
}

const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
    
  }

export default withRouter( connect(mapToStateProps,mapDispatchToProps) (Dashboard) );
