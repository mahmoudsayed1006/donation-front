import React ,{ Fragment }from 'react';
import './home.css';
import { Skeleton, message,Form, Input, Button,Popconfirm,Select,Icon,Modal} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import { MDBContainer,  MDBRow, MDBCol,MDBInput} from "mdbreact";
import {NavLink} from 'react-router-dom';
import {getUser} from '../../actions/AuthActions'
import {ChangeLanguage} from '../../actions/LanguageAction'
import Nav from '../../components/navbar/navbar';
import moment from 'moment';  

class Home extends React.Component {
    componentDidMount() {
      this.getCategories()
      this.getposts(1)
        window.scrollTo(0, 0)
      }

     constructor(props){
      super(props)
      console.log(this.props.currentUser)

      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }
   
    state = {
        categories:[],
        posts:[],
        loading:true,
        category:'All',
        modal1Visible: false,
       
      };
      getCategories = () =>{
          axios.get(`${BASE_END_POINT}categories`)
          .then(response=>{
              console.log('CATEGORIES  ',response.data.data)
              const cat = response.data.data;
              this.setState({categories:cat}) 
          })
          .catch(error=>{
            console.log('CATEGORIES ERROR  ',error.response)
          })
      }

      flag = 0;
       getposts = (page,category) => {
         let url =`${BASE_END_POINT}/posts?accept=false&page=${page}&limit={20}`;
         console.log(category)
         if(category){
           if(category == "All"){
            url = `${BASE_END_POINT}/posts?page=${page}&limit={20}`
           } else{
            url = `${BASE_END_POINT}/posts?category=${category}&page=${page}&limit={20}`
           
           }
         }
        
         axios.get(url)
         .then(response=>{
           console.log("ALL posts")
           console.log(response.data)
           
          
           this.setState({posts:response.data.data,loading:false})
         })
         .catch(error=>{
          // console.log("ALL posts ERROR")
           //console.log(error.response)
         })
       }
       changeHandler = (event )=> {
         console.log( event.target.value)
        this.setState({ category: event.target.value,loading:true} );
        this.getposts(1,event.target.value)
      };
      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }
      handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            //console.log('date ', date[2]  );
            const data = {
                description: values.description,
                category: values.category.key,
                                            
            }
            if(values.phone){
              data.phone = values.phone
            }
          
            let l = message.loading(allStrings.wait, 2.5)
            axios.post(`${BASE_END_POINT}posts`,JSON.stringify(data),{
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            })
            .then(response=>{
              console.log(allStrings.addDone)
                l.then(() => message.success('Add ', 2.5));
                this.setState({ modal1Visible:false ,loading:true});
                this.getposts(1)
                this.flag = -1
                this.props.form.resetFields()
            })
            .catch(error=>{
                console.log(error.response)
                l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
            })

          }
        });
        
      }
    render() {
        const {categories,posts} = this.state;
        const {isRTL} = this.props;
        const img ='https://image.flaticon.com/icons/png/512/149/149071.png'
        const { getFieldDecorator } = this.props.form;
        const Option = Select.Option;
        const{currentUser} = this.props

      return ( 
        <div style={{background:'#dcedc8'}} > 
            <Nav></Nav>
            <MDBContainer>
              <MDBRow>
                <div style={{width: '90%',marginTop: '20px'}}>
                  <p className="homeTitle">{allStrings.donationList}</p>
                  {this.props.currentUser.user.type =="VOLUNTEER" &&
                <Button style={{color: 'white', backgroundColor:'#187336',width: '120px',float: 'left',height: '37px'}}  onClick={() => this.setModal1Visible(true)}>{allStrings.addDonation}</Button>

                  }
                <select class="custom-select"
                style={{ width: '160px',float: 'right'}} 
                onChange={this.changeHandler}
                >
                  <option selected value="All">{allStrings.allCategory}</option>
                {categories.map(category=>(
                    <option value={category.id}>{isRTL?category.arabicname:category.categoryname} </option>
                ))}   
  
                </select>
                  </div>
                </MDBRow>
                <MDBRow>
                  
                      {this.state.loading == true &&
                      <div class="posts">    
                        <div style={{minHeight:600,display:'flex',justifyContent:'center',alignItems:'center'}} >
                          <Icon style={{color: '#fff',fontSize: '70px'}} type="loading" />

                        </div>
                      </div>
                      }
                     {this.state.loading == false && 
                    <div class="posts">
                    {posts.map(post=>(
                      <div className="post">
                        <MDBRow>
                        <MDBCol md="2">
                          <div className="pimg">
                            <img style={{borderColor:'transparent',height:'60px',width:'60px'}} src={ img}></img>
                          </div>
                        </MDBCol>
                        <MDBCol md="8">
                        <div className="desc">
                            <p className="thelabel">{allStrings.description}  </p>
                            <p className="thedata">{post.description}</p>
                            <p className="thelabel">{allStrings.phone }  </p>
                            <p className="thedata">{post.phone}</p>
                            <div style={{color: '#000',textAlign: 'right'}}>
                            
                              {allStrings.date} : {moment(post.createdAt).fromNow()}
                            </div>
                          </div>
                        </MDBCol>
                        </MDBRow>
                      </div>
                      
                      ))}
                    </div>
    }
              <Modal
                    title={allStrings.add}
                    visible={this.state.modal1Visible}
                    onOk={this.handleSubmit}
                    okText={allStrings.ok}
                    cancelText={allStrings.cancel}
                    onCancel={() => this.setModal1Visible(false)}
                  >
                    <Form onSubmit={this.handleSubmit} className="login-form">
                    <Form.Item>
                    {getFieldDecorator('description', {
                        rules: [{ required: true, message: allStrings.descriptionValid}],
                    })(
                        <Input placeholder={allStrings.description} />
                    )}
                    </Form.Item>
                                
                    <Form.Item hasFeedback >
                    {getFieldDecorator('phone', {
                        rules: [
                          {required: false, message: allStrings.phoneValid},
                        ],
                        
                    })(
                        <Input placeholder={allStrings.phone} />
                    )}
                    </Form.Item>
                    <Form.Item>
                      {getFieldDecorator('category', {
                          rules: [{ required: true, message: allStrings.categoryValid }],
                      })(
                          <Select labelInValue  
                          placeholder={allStrings.category}
                          style={{ width: '100%'}} >
                          {this.state.categories.map(
                              val=><Option value={val.id}>{isRTL?val.arabicname:val.categoryname} </Option>
                          )}
                          </Select>
                      )}
                      </Form.Item>
                  
                   
                    </Form>
                </Modal>
                </MDBRow>
            </MDBContainer> 
        </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
    getUser,
    ChangeLanguage
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(Home = Form.create({ name: 'normal_login' })(Home));
