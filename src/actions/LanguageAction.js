import {LANG} from './types';

export function ChangeLanguage(change){
    console.log("Lang action  ",change)
    return(dispatch) => {
        dispatch({type:LANG,payload:change})
    }
}