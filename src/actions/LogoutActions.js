import { LOGOUT,LOGOUT_LOADING,LOGOUT_LOADING_END } from "./types";
import { BASE_END_POINT } from '../config/URL';

import axios from 'axios';


export  function logout(FB_token, BE_token, history) {
   
    return  dispatch => {
        dispatch({type:LOGOUT_LOADING})
        axios.put(`${BASE_END_POINT}logout`, {}, {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${BE_token}`
          },
        }).then(response => {
            console.log('log out')
            console.log(response);
            
            localStorage.removeItem('@QsathaUser')
            history.push('/Login')       
            localStorage.clear();
            dispatch({ type: LOGOUT })
          
        }).catch(error => {
          localStorage.removeItem('@QsathaUser') 
          
          history.push('/Login')       
          localStorage.clear();
          window.location.reload();
          dispatch({ type: LOGOUT })
        });
        
    }
}